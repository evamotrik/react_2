import React, { Component } from 'react';

class Post extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: this.props.flag ? true : false
        }
    }

    componentWillReceiveProps(nextProps, nextContext){
        if(nextProps.flag !== this.state.isOpen){
          this.setState({isOpen: nextProps.flag})
        }
      }

    render() {
        const info = (
            <div>{this.props.post.info}</div>
        );
        return (
            <div>
                 <h1>{this.props.post.title}</h1>
                 <div><img src={this.props.post.pic} /></div>
                <button onClick={this.handleClick}>{this.state.isOpen ? "Свернуть" : "Развернуть"}</button>
                {this.state.isOpen && info}
            </div>
        )
    }

    handleClick = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
}

export default Post;
