import React, { Component } from 'react';
import './App.css';
import Post from './Post'
import { news } from "./News";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: news,
      inputValue: '',
    }
  }

  searchPost = () => {  
    this.setState({
      posts: this.state.posts.filter((item) => {
        return item.title.toLowerCase() === this.state.inputValue.toLowerCase()
      })
    })
  }

  handleChange = (event) => {
    this.setState({inputValue: event.target.value})
  }

  reversNews = () => {
    this.setState({
      posts: this.state.posts.reverse()
    })
  }

  refreshPage = () => {
    // window.location.reload(false);
    this.setState({
    posts: news
 })
  }

  render() {

    return (
      <div className="App">
        <header className="header">
          <h1>Самые красивые места мира</h1>
          <div className = "container">

            <button className="ReverseBtn" onClick={this.reversNews}>Фильтровать</button>
  
            <input className="SearchInput" type="text" placeholder="Искать" value = {this.state.inputValue}
             onChange={this.handleChange} />

            <button className="SearchBtn" onClick = {this.searchPost}>Найти</button>

            <button onClick = {this.refreshPage}>Сбросить</button>
          </div>
        </header>
        {this.state.posts.map((post, i) =>  i === 0 ? <Post key={post.id} post={post} i={i} flag /> : <Post key={post.id} post={post} i={i} />)}

      </div>
    );
  }
}

export default App;
